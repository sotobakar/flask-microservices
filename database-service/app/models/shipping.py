from app import db

class Shipping(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    city_from = db.Column(db.String(50), nullable=False)
    city_to = db.Column(db.String(50), nullable=False)
    courier = db.Column(db.String(30), nullable=False)
    fee = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Shipping from {} to {} via {}>'.format(self.city_from, self.city_to, self.courier)


