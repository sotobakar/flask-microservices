from flask import Blueprint, jsonify, request, current_app, g
from app.models.shipping import Shipping
from app import db

bp = Blueprint("api", __name__)

@bp.get("/shippings")
def get_shippings():
    return jsonify({"message": "Test DB Service"})

@bp.post("/shippings")
def create_shippings():
    try:
        data = request.get_json()

        if (data == None):
            raise ValueError('Request body is empty')

        return jsonify({"message": "Saved shipping to table."}), 201
    except BaseException as err:
        return jsonify({
            'message': '{}'.format(err),
            'errors': 'Error type {}'.format(type(err))
        }), 400

