# Microservices with Flask

## Requirements :
- Buatlah 3 server (gateway, server 1 dan server 2) menggunakan Huawei Cloud yang telah dibagikan akses-nya
- Server Gateway menghubungkan antara client (postman) dengan Server 1 dan Server 2. Server 1 berisi layanan koneksi ke RajaOngkir untuk mendapatkan ongkos kirim dari kota A ke kota B. sementara Server 2 berisi layanan koneksi ke local database (database bebas dibuat).
- Buatlah skema arsitektur seperti yang telah dijelaskan pada sesi kelas
- Tugas dapat di implementasikan dengan NodeJS atau Python

## Frameworks and Tools Used : 
- Python 3.6
- Flask Web Framework
- Pytest
- SQLite

## How to deploy :