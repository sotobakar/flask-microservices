from flask import Blueprint, jsonify, request, current_app, g
from app import cache
import requests

bp = Blueprint("api", __name__)


@bp.before_request
def before_request_func():
    g.base_url = "https://api.rajaongkir.com/starter"
    g.headers = {
        "key": current_app.config["API_KEY"],
        "Content-Type": "application/json",
    }


@bp.post("/shipping")
def count_shipping():
    request_data = request.get_json()

    # Post request to RajaOngkir API
    response = requests.post(
        "{}/cost".format(g.base_url), headers=g.headers, json=request_data
    )
    if response.status_code == 200:
        shipping_cost = response.json()["rajaongkir"]["results"][0]
        return jsonify({"message": "Shipping cost retrieved", "data": shipping_cost})
    else:
        return jsonify(response.json()["rajaongkir"]), response.status_code


# TODO: QUERYING
@bp.get("/cities")
def get_cities():
    if cache.get("cities") is None:
        response = requests.get("{}/city".format(g.base_url), headers=g.headers)
        if response.status_code == 200:
            cities = response.json()["rajaongkir"]["results"]
            cache.set("cities", cities)
            return jsonify({"message": "Cities retrieved", "data": cities})
        else:
            return jsonify({"message": "Data unavailable"}), 400
    else:
        cities = cache.get("cities")
        return jsonify({"message": "Cities retrieved", "data": cities})


# TODO: QUERYING
@bp.get("/provinces")
def get_provinces():
    if cache.get("provinces") is None:
        response = requests.get("{}/province".format(g.base_url), headers=g.headers)
        if response.status_code == 200:
            provinces = response.json()["rajaongkir"]["results"]
            cache.set("provinces", provinces)
            return jsonify({"message": "Provinces retrieved", "data": provinces})
        else:
            return jsonify({"message": "Data unavailable"}), 400
    else:
        provinces = cache.get("provinces")
        return jsonify({"message": "Provinces retrieved", "data": provinces})
